package com.epm.logic.pizzacomponents;

public enum Dough {
  THICK_CRUST("Thick"), THIN_CRUST("Thick");

  String crust;
  Dough(String crust) {
    this.crust = crust;
  }

  public String getCrust() {
    return crust;
  }
}
