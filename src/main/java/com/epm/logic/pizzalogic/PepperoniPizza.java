package com.epm.logic.pizzalogic;

import com.epm.backer.factory.LocationBaker;
import com.epm.logic.pizzacomponents.Dough;
import com.epm.logic.pizzacomponents.Souce;
import com.epm.logic.pizzacomponents.Toppings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.epm.backer.factory.LocationBaker.DNIPRO;
import static com.epm.backer.factory.LocationBaker.KYIV;
import static com.epm.backer.factory.LocationBaker.LVIV;
import static com.epm.logic.pizzacomponents.Dough.THICK_CRUST;
import static com.epm.logic.pizzacomponents.Dough.THIN_CRUST;
import static com.epm.logic.pizzacomponents.Souce.MARINARA;
import static com.epm.logic.pizzacomponents.Souce.PESTO;
import static com.epm.logic.pizzacomponents.Souce.PLUM_TOMATO;
import static com.epm.logic.pizzacomponents.Toppings.*;

public class PepperoniPizza implements Pizza{
  private Dough dough;
  private Souce souce;
  private List<Toppings> toppings = new ArrayList<>();
  private static Logger logger = LogManager.getLogger(PepperoniPizza.class);

  @Override
  public void prepare(LocationBaker locationBaker) {

    logger.trace("PIZZA PARAMETERS : ");
    if (locationBaker.equals(LVIV)) {
      this.dough = THICK_CRUST;
      logger.info(dough);
      this.souce = PLUM_TOMATO;
      logger.info(souce);
      toppings.add(PAPERONI);
      toppings.add(TOMATOS);
      toppings.add(ONION);
      toppings.add(CHEESE);
      toppings.add(MUSHROOMS);
      logger.info("Toppings of your pizza :" + toppings);
    }
    if (locationBaker.equals(KYIV)) {
      this.dough = THIN_CRUST;
      logger.info(dough);
      this.souce = PESTO;
      logger.info(souce);
      toppings.add(PAPERONI);
      toppings.add(TOMATOS);
      toppings.add(CHEESE);
      logger.info("Toppings of your pizza :" + toppings);
    }
    if (locationBaker.equals(DNIPRO)) {
      this.dough = THIN_CRUST;
      logger.info(dough);
      this.souce = MARINARA;
      logger.info(souce);
      toppings.add(PAPERONI);
      toppings.add(CHEESE);
      toppings.add(SALAMI);
      logger.info("Toppings of your pizza :" + toppings);
    }
  }

  @Override
  public void bake() {
    logger.info("Wait your pizza bake");
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void cut() {
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info("Your pizza cut");
  }

  @Override
  public void box() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info("Your pizza box");
  }

  @Override
  public String toString() {
    return "Your pizza is finish :" +
            " [" +
            "dough=" + dough +
            ", souce=" + souce +
            ", toppings=" + toppings
            + "]";
  }
}
