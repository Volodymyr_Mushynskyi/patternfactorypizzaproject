package com.epm.logic.pizzalogic;

import com.epm.backer.factory.LocationBaker;

public interface Pizza {
  void prepare(LocationBaker locationBaker);
  void bake();
  void cut();
  void box();
}
