package com.epm.menu.prepareorder;

import com.epm.backer.factory.DniproBacker;
import com.epm.backer.factory.KyivBacker;
import com.epm.backer.factory.LocationBaker;
import com.epm.backer.factory.LvivBacker;
import com.epm.logic.pizzalogic.CheesePizza;
import com.epm.logic.pizzalogic.Pizza;
import com.epm.order.PizzaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epm.backer.factory.LocationBaker.DNIPRO;
import static com.epm.backer.factory.LocationBaker.KYIV;
import static com.epm.backer.factory.LocationBaker.LVIV;
import static com.epm.order.PizzaType.CHEESE;

public class Choosing {
  private static Logger logger = LogManager.getLogger(Choosing.class);

  public void choose(LocationBaker locationBaker, PizzaType pizzaType){

    if(locationBaker.equals(LVIV)){
      LvivBacker lvivBacker = new LvivBacker();
      Pizza pizza = lvivBacker.createPizza(LVIV, CHEESE);
      logger.info(pizza);
    }

    if(locationBaker.equals(KYIV)){
      KyivBacker kyivBacker = new KyivBacker();
      Pizza pizza = kyivBacker.createPizza(KYIV, CHEESE);
      logger.info(pizza);
    }

    if(locationBaker.equals(DNIPRO)){
      DniproBacker dniproBacker = new DniproBacker();
      Pizza pizza = dniproBacker.createPizza(DNIPRO, CHEESE);
      logger.info(pizza);
    }
  }
}
