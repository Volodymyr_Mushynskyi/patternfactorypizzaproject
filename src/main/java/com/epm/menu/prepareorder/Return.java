package com.epm.menu.prepareorder;

import com.epm.backer.factory.LocationBaker;
import com.epm.order.PizzaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epm.backer.factory.LocationBaker.DNIPRO;
import static com.epm.backer.factory.LocationBaker.KYIV;
import static com.epm.backer.factory.LocationBaker.LVIV;
import static com.epm.order.PizzaType.*;

public class Return {
  private static Logger logger = LogManager.getLogger(Return.class);

  public LocationBaker getCity(String city) {
    if(city.equalsIgnoreCase("LVIV")){
      return LVIV;
    }if(city.equalsIgnoreCase("KYIV")){
      return KYIV;
    }if(city.equalsIgnoreCase("DNIPRO")){
      return DNIPRO;
    }
    return null;
  }
  public PizzaType getPizzaType(String type) {
    if(type.equalsIgnoreCase("CHEESE")){
      return CHEESE;
    }if(type.equalsIgnoreCase("CLAM")){
      return CLAM;
    }if(type.equalsIgnoreCase("PEPPERONI")){
      return PEPPERONI;
    }if(type.equalsIgnoreCase("VEGGIE")){
      return VEGGIE;
    }
    return null;
  }

  public void getAviaibleOrder() {
   logger.info("Aviable type of pizza : ");
   logger.info("CHEESE , CLAM , PEPPERONI , VEGGIE");
  }
}
