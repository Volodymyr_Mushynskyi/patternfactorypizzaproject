package com.epm.menu;

public interface Printable {
  void print();
}
