package com.epm.menu;

import com.epm.menu.prepareorder.Choosing;
import com.epm.menu.prepareorder.Return;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
  private Choosing choosing;
  private Return returnParam;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private Logger logger = LogManager.getLogger(View.class);

  public View() {
    choosing = new Choosing();
    returnParam = new Return();
    menu = new LinkedHashMap<>();
    menu.put("1", "Press  1 - execute make order");
    menu.put("0", "Press  0 - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::makeOrder);
  }

  private void makeOrder() {
    returnParam.getAviaibleOrder();
    String type;
    String city;
    logger.info("Write what type of pizza do you want :");
    type =  input.nextLine();
    logger.info("Write city in what ypu want get delivery :");
    city =  input.nextLine();
    choosing.choose(returnParam.getCity(city),returnParam.getPizzaType(type));
  }

  private void printMenuAction() {
    System.out.println("--------------MENU-----------\n");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void showMenu() {
    String keyMenu;
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("0"));
  }
}
