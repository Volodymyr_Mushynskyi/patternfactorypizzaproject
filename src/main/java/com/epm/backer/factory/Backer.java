package com.epm.backer.factory;

import com.epm.logic.pizzalogic.Pizza;
import com.epm.order.PizzaType;

public abstract class Backer {
  public abstract Pizza orderPizza(PizzaType pizzaType);

  public Pizza createPizza(LocationBaker locationBaker, PizzaType pizzaType){
    Pizza pizza = orderPizza(pizzaType);
    pizza.prepare(locationBaker);
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }
}
