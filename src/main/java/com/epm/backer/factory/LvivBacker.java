package com.epm.backer.factory;

import com.epm.logic.pizzalogic.CheesePizza;
import com.epm.logic.pizzalogic.ClamPizza;
import com.epm.logic.pizzalogic.PepperoniPizza;
import com.epm.logic.pizzalogic.Pizza;
import com.epm.order.PizzaType;

import static com.epm.order.PizzaType.*;

public class LvivBacker extends Backer {

  public Pizza orderPizza(PizzaType pizzaType) {
    Pizza pizza =  null;
    if(pizzaType.equals(CHEESE)) {
      pizza = new CheesePizza();
    }
    if(pizzaType.equals(CLAM)) {
      pizza = new ClamPizza();
    }
    if(pizzaType.equals(PEPPERONI)) {
      pizza = new PepperoniPizza();
    }
    if(pizzaType.equals(VEGGIE)) {
      pizza = new ClamPizza();
    }
    return pizza;
  }
}
